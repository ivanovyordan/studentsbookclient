exports.helpers = {
	renderScriptsTags: function(scripts)
	{
		if(scripts === undefined) {
			return '';
		}
		
		return scripts.map(function(script){
			return '<script type="text/javascript" src="/javascripts/' + script + '"></script>';
		}).join('\n');
	}
};

exports.dynamicHelpers = {
	session: function(request, response) { return request.session; },
	scripts: function() { return ['jquery.js', 'bootstrap.min.js', 'less.js']; },
	
	flashMessages: function(request)
	{
		var html = '';
		['success', 'info', 'warning', 'error'].forEach(function(type) {
			var messages = request.flash(type);
			if(messages.length > 0) {
				html += new FlashMessage(type, messages).toHTML();
			}
		});
		
		return html;
	}
};


function FlashMessage(type, messages)
{
	this.type = type;
	this.messages = typeof messages === 'string' ? [messages] : messages;
}

FlashMessage.prototype = {
	get stateClass()
	{
		console.log()
		return (this.type === 'warning') ? '' : ' alert-' + this.type;
	},
	get heading()
	{
		switch(this.type) {
			case 'success':
				return 'Успех!';
			case 'info':
				return 'Информация:';
			case 'warning':
				return 'Внимание!';
			case 'error':
				return 'Грешка';
			default:
				return '';
		}
	},
	
	toHTML: function() {
		var _this = this;
		var html = '';
		_this.messages.forEach(function(message){
			html += '<div class="alert' + _this.stateClass + '">\n' +
			'\t<a class="close" data-dismiss="alert" href="#">×</a>\n'+
			'\t<h4 class="alert-heading">' + _this.heading + '</h4>\n'+
			'\t' + message + '\n' +
			'</div>\n';
		});
		return html;
	}
};