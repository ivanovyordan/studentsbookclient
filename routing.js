module.exports = function(app)
{
	var fs = require('fs');
	
	// Routes
	app.all('/', function(request, response){
		var controller = require(__dirname + '/routes/user');
		controller.index(request, response);
	});
	
	// Routes
	app.all('/login', function(request, response){
		var controller = require(__dirname + '/routes/user');
		controller.login(request, response);
	});
	
	try {
		files = fs.readdirSync(__dirname + '/routes');
		files.forEach(function(file){
			file = file.split('.')[0];
			
			app.post(file + '/new' + file, function(){
				var controller = require(__dirname + '/routes/' + file);
				controller.create();
			});
			
			app.get('/' +file, function(){
				var controller = require(__dirname + '/routes/' + file);
				console.log('here');
				controller.dashboard();
			});
			
			app.get('/' + file + '/:id', function(){
				var controller = require(__dirname + '/routes/' + file);
				controller.view();
			});
			
			app.get('/' + file + '/:id/edit', function(){
				var controller = require(__dirname + '/routes/' + file);
				controller.edit();
			});
			
			app.get('/' + file + '/:id/delete', function(){
				var controller = require(__dirname + '/routes/' + file);
				controller.edit();
			});
		});
	} catch(error) {
		console.log(error);
		process.exit();
	}
};