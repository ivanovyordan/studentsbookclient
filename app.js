/**
 * Module dependencies.
 */
var express = require('express'),
	expose = require('express-expose'),
	helpers = require(__dirname + '/helpers');


GLOBAL.config = require(__dirname + '/config'),
GLOBAL.rest = require('restler');
GLOBAL.when = require('node-promise/promise').when;

app = module.exports = express.createServer();

// Configuration
app.configure(function(){
	app.set('views', __dirname + '/views');
	app.set('view engine', 'ejs');
	//app.use(express.logger());
	app.use(express.bodyParser());
	app.use(express.cookieParser());
	app.use(express.methodOverride());
	app.use(express.session({ secret: 'password' }));
	app.use(app.router);
	app.use(express.static(__dirname + '/public'));
	app.helpers(helpers.helpers);
	app.dynamicHelpers(helpers.dynamicHelpers);
});

app.configure('development', function(){
	app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
	app.use(express.errorHandler());
});

/* Routings */
require(__dirname + '/routing')(app);

app.listen(config.app.port, function(){
	console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});
