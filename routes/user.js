exports.index = function(request, response)
{
	if(request.session.user === undefined) {
		exports.login(request, response);
	} else {
		var location = '';
		switch(request.session.user.group_id) {
			case 1: location = '/inspector'; break;
			case 2: location = '/trainer'; break;
			case 3: location = '/student'; break;
			default: location = '';
		}
		response.redirect(location);
	}
};

exports.login = function(request, response)
{
	if(request.session.user !== undefined) {
		response.redirect('/');
	}
	
	if(request.body.submit === undefined) {
		response.render('users/login', {title: 'Вход'});
		return;
	}
	
	if(request.body.username === '' || request.body.password === '') {
		request.flash('error', 'Моля въведете потребителско име и парола.');
		response.render('users/login', {title: 'Вход'});
		return;
	}
	
	rest.post(config.server.url + '/login', {data: {
		username: request.body.username,
		password: request.body.password
	}}).on('success', function(user) {
		request.session.user = user;
		request.rediredct('/');
	}).on('fail', function(error, result) {
		var errorMessage = '';
		switch(result.statusCode) {
			case 412:
				errorMessage = 'Моля въведете потребителско име и парола.';
				break;
			case 401:
				errorMessage = 'Невалидни данни за вход.';
				break;
			default:
				errorMessage = error;
		}
		
		request.flash('error', errorMessage);
		response.render('users/login', {title: 'Вход'});
	});
};