module.exports = {
	app: {
		port: 3000
	},
	server: {
		url: 'http://localhost:8000/',
		tokenHeader: 'X-USER-TOKEN'
	}
};